<?php
/**
 * @file
 * This file is the endpoint for incoming webhook API calls from Bitbucket
 * Reacts to these calls by sending notifications to Slack as well as
 * copying actions from the Bitbucket repo to the Acquia repo.
 */

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

require_once('inc/slack.php');
require_once('inc/bitbucket.php');
require_once('inc/logger.php');
require_once('inc/lock.php');
require_once('inc/git.php');

// Raw bitbucket data - todo detect if this message really comes from bitbucket
$bitbucketdata = json_decode(file_get_contents('php://input'), TRUE);


// Read the bitbucket event key
if (isset($_SERVER['HTTP_X_EVENT_KEY'])) {
  $event_key = $_SERVER['HTTP_X_EVENT_KEY'];

  // Pull Request: Created, approved, or merged.
  if (stristr($event_key, 'pullrequest')) {
    $b = new BitbucketPRPayload($bitbucketdata, $event_key);

    // Determine a slack channel based on branch
    switch ($b->getDestinationBranch()) {
      //case 'master':
      //  $channel = 'deployment';
      //  break;

      default:
        $channel = 'webdev-notifications';
        break;
    }

    // Get branch name
    $branch = $b->getDestinationBranch();

    // Make up a bot name based on branch
    $botname = $branch . 'bot';

    // Check for release branch
    if (stristr($b->getDestinationBranch(), 'release/')) {
      $botname = 'releasebot';
    }

    // Send a slack message based on PR ac
    switch ($b->getAction()) {
      // PR Created
      case 'created':
        $message = ":smile: New pull request created for branch $branch by _" . $b->getActor()->getUsername() . '_';

        // Custom CP message
        if($branch == 'cp') {
          $message = ":smile: Hey @jmdodge - New pull request created for branch $branch by _" . $b->getActor()->getUsername() . '_';
        }

        break;

      // PR Approved
      case 'approved':
        $message = ":smile: Pull request *approved* on branch $branch by _" . $b->getActor()->getUsername() . '_';
        break;

      // PR Merged
      case 'fulfilled':
        // Custom master messages - code being deployed
        if($branch == 'master') {
          $channel = 'deployment';
          $message = "@here Hey, looks like code is getting ready to be deployed to production. You guys are awesome! :bananadance: :sunglasses: Pull request *merged* on branch $branch by _" . $b->getActor()->getUsername() . '_';
        }
        else {
          $message = "@here :sunglasses: Pull request *merged* on branch $branch by _" . $b->getActor()->getUsername() . '_';
        }
        break;

      // PR Rejected
      case 'rejected':
        $message = ":slightly_frowning_face: Pull request *declined* on branch $branch by _" . $b->getActor()->getUsername() . '_';
        break;
    }

    // Send to slack
    if (!empty($message)) {
      slack($message, $channel, SLACK_ICON, $botname, array($b->getSlackAttachment()));
    }
  }

  // Push
  if (stristr($event_key, 'push')) {
    $b = new BitbucketPush($bitbucketdata);

    // Send a slack message and push the branch to Acquia
    if($b->getPushObjectType() == 'branch') {
      $message = 'A branch has been pushed to the repository';
      $channel = 'webdev-notifications';
      $botname = 'Repo Push Bot';

      switch($b->getType()) {
        case BitbucketPush::TYPE_CREATE:
          $message = "*{$b->getBranchName()}* branch has been created in the repository.";
          break;
        case BitbucketPush::TYPE_UPDATE:
          $message = "*{$b->getBranchName()}* branch has been updated in the repository.";
          break;
        case BitbucketPush::TYPE_DELETE:
          $message = "*{$b->getBranchName()}* branch has been deleted from the repository.";
          break;
      }

      // Mirror to acquia
      gitMirrorBranch($b);

      $att = array(
        'title'       => $message,
        //'title_link'  => $this->getUrl(),
        'text'        => logger::getMessagesFormatted(),
        "author_name" => $b->getActor()->getUsername(),
        "author_link" => $b->getActor()->getUrl(),
        "author_icon" => $b->getActor()->getIcon(),
        //'footer'      => "Sync output: \n" . logger::getMessagesFormatted(),
        /*'fields' => array(
          array(
            'title' => 'Sync output',
            'value' => logger::getMessagesFormatted(),
            'short' => FALSE
          ),
        ),*/
      );

      // Send slack message
      slack($message, $channel, SLACK_ICON, $botname, array($att));
    }

  }
}
else {
 print 'No Bitbucket event key detected';
}

exit();


function debugRequest() {
  // Print REQUEST
  print '<pre>';
  print_r($_REQUEST);
  print '</pre>';

  print '<hr>';

  // Print SERVER
  print '<pre>';
  print_r($_SERVER);
  print '</pre>';
}
