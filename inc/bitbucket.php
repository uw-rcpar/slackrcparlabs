<?php

/**
 * Class BitbucketPRPayload
 * Interacts with the pull request JSON payload from Bitbucket's incoming webhook
 */
class BitbucketPRPayload {
  private $payload;
  private $event_key;

  /**
   * BitbucketPRPayload constructor.
   * @param array $payload
   * - JSON decoded data from bitbucket
   */
  function __construct($payload, $event_key) {
    $this->payload = $payload;
    $this->event_key = $event_key;
  }

  /**
   * @return string
   * rejected
   * fulfilled
   * approved
   */
  function getAction() {
    $parts = explode(':', $this->event_key);
    return $parts[1];
  }

  function getSourceBranch() {
    return $this->payload['pullrequest']['source']['branch']['name'];
  }

  function getDestinationBranch() {
    return $this->payload['pullrequest']['destination']['branch']['name'];
  }

  function getUrl() {
    return $this->payload['pullrequest']['links']['html']['href'];
  }

  function getTitle() {
    return $this->payload['pullrequest']['title'];
  }

  /**
   * Returns the text description of the pull request.
   * @return string
   */
  function getDescription() {
    return $this->payload['pullrequest']['description'];
  }

  /**
   * Returns the original creator of the pull request.
   * @return BitbucketUser
   */
  function getAuthor() {
    return new BitbucketUser($this->payload['pullrequest']['author']);
  }

  /**
   * The "actor" is the person that generated the action. They may not be the same as the author of a pull request.
   * @return BitbucketUser
   */
  function getActor() {
    return new BitbucketUser($this->payload['actor']);
  }


  function getSlackAttachment() {
    $att = array(
      'title'       => $this->getTitle(),
      'title_link'  => $this->getUrl(),
      'text'        => $this->getDescription(),
      "author_name" => $this->getAuthor()->getUsername(),
      "author_link" => $this->getAuthor()->getUrl(),
      "author_icon" => $this->getAuthor()->getIcon(),
      'footer'      => 'Source branch: ' . $this->getSourceBranch(),
    );

    return $att;
  }
}

class BitbucketUser {
  private $payload;

  function __construct($payload) {
    $this->payload = $payload;
  }

  function getUsername() {
    return $this->payload['username'];
  }

  function getUrl() {
    return $this->payload['links']['html']['href'];
  }

  function getIcon() {
    return $this->payload['links']['avatar']['href'];
  }
}

class BitbucketPush {
  private $pushChanges;
  private $payload;
  const TYPE_CREATE = 'c';
  const TYPE_UPDATE = 'u';
  const TYPE_DELETE = 'del';

  function __construct($payload) {
    if(isset($payload['push']) && isset($payload['push']['changes'][0])) {
      $this->pushChanges = $payload['push']['changes'][0];
    }

    $this->payload = $payload;
  }

  function getPushObjectType() {
    switch ($this->getType()) {
      case self::TYPE_CREATE:
      case self::TYPE_UPDATE:
        return $this->pushChanges['new']['type'];
        break;
      case self::TYPE_DELETE:
        return $this->pushChanges['old']['type'];
        break;
    }

    return '';
  }

  function getType() {
    // When there is a new reference and not an old reference, a branch is being created
    if(isset($this->pushChanges['new']) && !isset($this->pushChanges['old'])) {
      return self::TYPE_CREATE;
    }

    // When there is an old and new reference, a branch is being updated
    if(isset($this->pushChanges['new']) && isset($this->pushChanges['old'])) {
      return self::TYPE_UPDATE;
    }

    // When there is an old but not a new reference, a branch is being deleted
    if(!isset($this->pushChanges['new']) && isset($this->pushChanges['old'])) {
      return self::TYPE_DELETE;
    }

    return '';
  }

  function getBranchName() {
    switch ($this->getType()) {
      case self::TYPE_CREATE:
      case self::TYPE_UPDATE:
        return $this->pushChanges['new']['name'];
        break;
      case self::TYPE_DELETE:
        return $this->pushChanges['old']['name'];
        break;
    }

    return '';
  }

  /**
   * The "actor" is the person that generated the action. They may not be the same as the author of a pull request.
   * @return BitbucketUser
   */
  function getActor() {
    return new BitbucketUser($this->payload['actor']);
  }
}