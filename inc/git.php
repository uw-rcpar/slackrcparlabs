<?php
/**
 * @file
 * Function that clones branch actions from one repo (bitbucket) to another (acquia)
 * Generic custom class for interacting with a custom repository.
 */

// Logging helper class
require_once('logger.php');

// Location on the local file system of our git repo
define('GIT_LOCAL_REPO', '/home/ec2-user/dingo');

/**
 * Mirrors an action on a branch in Bitbucket to the corresponding branch in Acquia.
 * E.g., If a branch is created, updated, or deleted in Bitbucket, we'll do the same in
 * Acquia.
 * @param BitbucketPush $b
 */
function gitMirrorBranch(BitbucketPush $b) {
  $g = new git(GIT_LOCAL_REPO);

  // Before running any git commands, we need to make sure we're not trying to operate at the
  // same time as another process
  $lock = new lock();
  if (!$lock->isAvailable()) {
    logger::log('Check lock', 'Another process is using git, waiting to acquire lock');
    if ($lock->waitAndAcquire()) {
      logger::log('Acquire lock', 'Waited and was able to acquire lock');
    }
    else {
      logger::log('Acquire lock fail', 'Waited but was NOT able to acquire lock. Something might be stuck');
      return;
    }
  }
  else {
    logger::log('Get lock', 'Git was free to use, acquiring lock');
    $lock->waitAndAcquire();
  }

  // Fetch the latest
  $g->fetch();
  $g->fetch('acquia');

  // This is the branch we're operating on
  $branchname = $b->getBranchName();

  switch ($b->getType()) {

    // CREATE a new branch
    case BitbucketPush::TYPE_CREATE:
      $g->runCmd("git checkout -b $branchname origin/$branchname");
      $g->runCmd("git pull origin $branchname"); // just in case the branch was already there locally
      $g->pushBranch($branchname, 'acquia');
      break;

    // UPDATE a branch
    case BitbucketPush::TYPE_UPDATE:
      // Checkout locally
      $g->checkout($branchname);

      // Update from origin
      $g->runCmd("git pull origin $branchname");

      // Push to Acquia
      $g->pushBranch($branchname, 'acquia');
      break;

    // DELETE a branch
    case BitbucketPush::TYPE_DELETE:
      // Checkout master in case we have the branch we're trying to delete checked out.
      $g->checkout('master');

      $g->deleteLocalBranch($branchname);
      $g->deleteRemoteBranch($branchname, 'acquia');
      break;
  }

  // Cleanup
  $g->runCmd("git fetch --all --prune");

  // Release the lock
  logger::log('Release lock', 'Released lock');
  $lock->release();
}

class git {
  protected $rootDir;

  // The output of the last command executed
  public $lastOutput;

  /**
   * git constructor.
   * @param string $repoDir
   * - Absolute path on the local file system to the root directory of this git repo
   */
  public function __construct($repoDir) {
    $this->rootDir = $repoDir;
  }

  /**
   * Change directory to the root dir of the repo
   */
  function chdir() {
    chdir($this->rootDir);
  }

  function fetch($remotename = '') {
    return $this->runCmd("git fetch $remotename");
  }

  /**
   * Checkout a branch locally
   * @param string $branchname
   */
  function checkout($branchname) {
    return $this->runCmd("git checkout $branchname");
  }

  function pushBranch($branchname, $remotename) {
    return $this->runCmd("git push $remotename $branchname");
  }

  function deleteLocalBranch($branchname) {
    return $this->runCmd("git branch -D $branchname");
  }

  function deleteRemoteBranch($branchname, $remotename) {
    return $this->runCmd("git push $remotename --delete $branchname");
  }

  function runCmd($command) {
    // Save current working directory and switch to the repo's dir
    $current = getcwd();
    chdir($this->rootDir);

    $orig_cmd = $command;

    // Without setting the HOME environment variable, git tries to read config files from the root user's
    // home dir and gets permission errors.
    // https://serverfault.com/questions/179833/apache2-user-home-directory-lock-to-root
    $command = 'HOME="/home/ec2-user" && ' . $command;

    // Capture the standard out of the command
    $command .= ' 2>&1';

    // Execute the command and store its output
    $this->lastOutput = shell_exec($command);

    // Restore working directory
    chdir($current);

    // Log and return
    logger::log($orig_cmd, $this->lastOutput);
    return $this->lastOutput;
  }
}
