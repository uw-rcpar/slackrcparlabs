<?php

/**
 * Class logger
 * Simple logging helper class
 */
class logger {
  public static $logMessages = array();

  public static function log($cmd, $msg) {
    self::$logMessages[] = array('cmd' => $cmd, 'msg' => $msg);
  }

  public static function printAll() {
    print '<pre>';
    print_r(self::getMessagesFormatted());
    print '</pre>';
  }

  public static function fileDump() {
    $dateString = date("Y-m-d_H-i-s", $_SERVER['REQUEST_TIME']);
    file_put_contents(__DIR__ . "/log-$dateString.txt", print_r(self::getMessagesFormatted(), TRUE));
  }

  public static function getMessagesFormatted() {
    $output = '';
    foreach (self::$logMessages as $logMessage) {
      $output .= 'CMD: ' . $logMessage['cmd'] . "\n";
      $output .= 'Result: ' . $logMessage['msg'] . "\n";
      $output .= "----------\n";
    }

    return $output;
  }
}