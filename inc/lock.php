<?php

/**
 * Class lock
 * Simple, file based semaphore/locking system
 */
class lock {
  // Wait time in seconds
  protected $waitTime = 3;

  // How many times we'll try to wait for the lock
  protected $waitAttempts = 10;

  // How many seconds until an acquired lock expires
  protected $expiration = 30;

  // Location of the semaphore file
  protected $location;

  // Unique name for the lock
  protected $lockname;

  public $logOutput = '';

  /**
   * lock constructor.
   * Sets lockname and semaphore file location
   * @param string $lockname
   * @param string $location
   */
  public function __construct($lockname = 'default', $location = '/var/www/html/semaphore.txt') {
    $this->lockname = $lockname;
    $this->location = $location;
  }

  /**
   * Acquire a lock, waiting for an existing lock to expire if necessary
   * @return bool
   * TRUE if a lock could be acquired, FALSE if not
   */
  public function waitAndAcquire() {
    if ($this->isAvailable()) {
      $this->acquire();
      return TRUE;
    }
    else {
      // Wait until we can acquire the lock
      $canAcquire = $this->wait();
      if ($canAcquire) {
        // Got it
        $this->acquire();
        return TRUE;
      }
      // We waited as long as we could but didn't get the lock
      else {
        return FALSE;
      }
    }
  }

  /**
   * Determine if a lock is available to acquire
   * @return bool
   */
  public function isAvailable() {
    $lockTimeStamp = $this->getLock();

    // No lock exists, it's available
    if(empty($lockTimeStamp)) {
      return TRUE;
    }
    // A lock exists...
    else {
      // It's expired, the lock is available
      if (time() > $lockTimeStamp) {
        // Release the lock first because it has expired
        $this->logOutput .= "Found expired lock and released." . time() . " is greater than lock exp.: $lockTimeStamp \n";
        $this->release();
        return TRUE;
      }
      // Lock is present and not expired, we can't acquire
      else {
        $this->logOutput .= "lock NOT available\n";
        return FALSE;
      }
    }
  }

  /**
   * Write the lock into our semaphore file
   * @return NULL
   */
  protected function acquire() {
    $this->logOutput .= "acquiring {$this->lockname}\n";
    // Generate the output for the lock in the format of: lockname|expirationTimestamp
    $lockContent = $this->lockname . '|' . (time() + $this->expiration) . "\n";
    file_put_contents($this->location, $lockContent, FILE_APPEND);
  }

  /**
   * Search the semaphore file for the lock and return its expiration timestamp, or FALSE if not found
   * @return int|bool
   */
  protected function getLock() {
    $f = file($this->location);

    $this->logOutput .= "searching for lock name: {$this->lockname}\n";
    $this->logOutput .= "getlock: all lines - " . print_r($f, true);

    foreach ($f as $line) {
      if(stristr($line, $this->lockname)) {
        $pieces = explode('|', $line);
        $timestamp = $pieces[1];
        $this->logOutput .= "found lock, expires: $timestamp \n";
        return $timestamp;
      }
    }

    // Couldn't find lock
    $this->logOutput .= "did not find lock: {$this->lockname}\n";
    return FALSE;
  }

  /**
   * Wait as many times as necessary to acquire a lock.
   * Max wait = waitTime * waitAttempts
   * @return bool
   * TRUE if the lock became available, FALSE if it didn't
   */
  protected function wait() {
    for ($i = 0; $i < $this->waitAttempts; $i++) {
      $this->logOutput .= "waiting\n";
      sleep($this->waitTime);
      if ($this->isAvailable()) {
        return TRUE;
      }
      // Not available, sleep for another cycle...
    }
    // We waited as long as we could but a lock exists and hasn't expired
    return FALSE;
  }

  /**
   * Release our lock by removing it from the semaphore table
   * @return NULL
   */
  public function release() {
    $this->logOutput .= "releasing lock: {$this->lockname}\n";
    $newlines = array();
    $f = file($this->location);

    // Rebuild each line of the semaphore file
    foreach ($f as $line) {
      $line = trim($line);

      // Don't re-add the lock into the file contents
      if(stristr($line, $this->lockname)) {
        continue;
      }

      // Don't add blank lines
      if (empty($line)) {
        continue;
      }

      $newlines[] = $line;
    }

    // Add a newline to the end so the next lock can start on a new line
    $newlines[] = "\n";

    // Write the entire file contents without the lock that is being released
    file_put_contents($this->location, implode("\n", $newlines), LOCK_EX);
  }
}