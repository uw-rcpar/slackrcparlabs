<?php
/**
 * @file
 * Config and helper function for sending a Slack message via the webhook API
 */

// See https://rcpar.slack.com/apps/A0F7XDUAZ-incoming-webhooks
// and bitbucket side: https://bitbucket.org/RCPAR/dingo/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin
const SLACK_WEBHOOK = 'https://hooks.slack.com/services/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';

// Roger CPA favicon
const SLACK_ICON = 'https://www.rogercpareview.com/sites/default/files/favicon.png';


/**
 * Send a message to Slack
 * @param $message
 * @param $room
 * @param string $icon
 * @param string $username
 * @param array $attachments
 * @return mixed
 */
function slack($message, $room, $icon = ":smile:", $username = 'Bitbucket bot', $attachments = array()) {
  $data = "payload=" . json_encode(
      array(
        'channel'     => "#{$room}",
        'text'        => $message,
        'username'    => $username,
        'icon_url'    => $icon,
        'attachments' => $attachments,
        'link_names'  => 1,
      )
    );

  // Make a cURL request to a custom webhook configured from Slack
  $ch = curl_init(SLACK_WEBHOOK);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  $result = curl_exec($ch);
  curl_close($ch);

  return $result;
}