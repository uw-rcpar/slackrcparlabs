<?php

/**
 * @file
 * This file is just for testing and fiddling around with the locking system via lock.php
 */

require_once('inc/lock.php');

$lock = new lock('testlock');
$lock->waitAndAcquire();
$lock->release();

$lock2 = new lock('otherlock');
$lock2->waitAndAcquire();
$lock2->release();

$lock3 = new lock();
$lock3->waitAndAcquire();
$lock3->release();

print $lock->logOutput;
print $lock2->logOutput;
print $lock3->logOutput;